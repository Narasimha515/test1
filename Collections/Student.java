package Collections;

public class Student implements Comparable {
	int rollNo;
	String Name;
	float perc;
	public Student(int rollNo, String Name, float perc) {
		super();
		this.rollNo = rollNo;
		this.Name = Name;
		this.perc = perc;
	}
	@Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		Student st=(Student)obj;
		if(rollNo==st.rollNo)
		return 0;
		else if(rollNo>st.rollNo)
			return 1;
		else
			return -1;
		
	}
}