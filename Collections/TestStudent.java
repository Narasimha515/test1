package Collections;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class TestStudent {
public static void main(String args[]) {
	ArrayList <Student> set=new ArrayList();
	set.add(new Student(2,"narasimha",156));
	set.add(new Student(8,"naga",156));
	set.add(new Student(4,"ganesh",90));
	Collections.sort(set);
	System.out.println("rollno wise sorting");
	Iterator itr=set.iterator();
	while(itr.hasNext()) {
		Student st=(Student)itr.next();
	System.out.println(st.rollNo+" "+st.Name+" "+st.perc);
	}
	Collections.sort(set,new Perccomparator());
	System.out.println("perc wise sorting");
	Iterator itr1=set.iterator();
	while(itr1.hasNext()) {
		Student st=(Student)itr1.next();
	System.out.println(st.rollNo+" "+st.Name+" "+st.perc);
	}
	Collections.sort(set,new Namecomparator());
	System.out.println("Name wise sorting");
	Iterator itr3=set.iterator();
	while(itr3.hasNext()) {
		Student st=(Student)itr3.next();
	System.out.println(st.rollNo+" "+st.Name+" "+st.perc);
	
}
}
}